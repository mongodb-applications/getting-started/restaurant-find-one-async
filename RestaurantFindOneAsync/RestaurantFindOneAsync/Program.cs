﻿//using System.Collections;
//using System.Collections.ObjectModel;
using MongoDB.Bson;
//using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace RestaurantFindOneAsync;

public class FindOneAsync
{
    private static IMongoCollection<Restaurant>? restaurantsCollection;
    private const string MongoConnectionString = "mongodb+srv://spravce:JYHoYzXaRFMvZah5@learn-mongodb-cluster.lkbhbsw.mongodb.net/?retryWrites=true&w=majority";

    public static async Task Main(string[] args)
    {

        if (string.IsNullOrEmpty(MongoConnectionString))
        {
            Environment.Exit(0);
        }    

        Setup(); 

        // Find one document using builders
        var buildersDocument = await FindOneRestaurantBuilderAsync();
        Console.WriteLine("Finding a document with builders...");
        Console.WriteLine(buildersDocument.ToBsonDocument());

        // Extra space for console readability
        Console.WriteLine();

        // Find one document using LINQ
        var linqDocument = await FindOneRestaurantLinqAsync();
        Console.WriteLine("Finding a document with LINQ...");
        Console.WriteLine(linqDocument.ToBsonDocument());
    }

    private static async Task<Restaurant> FindOneRestaurantBuilderAsync()
    {
        // start-find-builders
        FilterDefinition<Restaurant> filter = Builders<Restaurant>.Filter
            .Eq(r => r.Name, "Bagels N Buns"); // var original

        return await restaurantsCollection.Find(filter).FirstOrDefaultAsync();
        // end-find-builders
    }

    private static async Task<Restaurant> FindOneRestaurantLinqAsync()
    {
        // start-find-linq
        return await restaurantsCollection.AsQueryable()
            .Where(r => r.Name == "Bagels N Buns").FirstOrDefaultAsync();
        // end-find-linq
    }

    private static void Setup()
    {
        // This allows automapping of the camelCase database fields to our models. 
        ConventionPack camelCaseConvention = new ConventionPack { new CamelCaseElementNameConvention() };
        ConventionRegistry.Register("CamelCase", camelCaseConvention, type => true);

        // Establish the connection to MongoDB and get the restaurants database
        IMongoClient mongoClient = new MongoClient(MongoConnectionString); //imongoclient?
        IMongoDatabase restaurantsDatabase = mongoClient.GetDatabase("sample_restaurants"); //imongodatabase, var
        restaurantsCollection = restaurantsDatabase.GetCollection<Restaurant>("restaurants"); //imongocollection, var
    }
}