﻿using MongoDB.Bson.Serialization.Attributes;

namespace RestaurantFindOneAsync;

public class Address
{
    public string? Building { get; set; }

    [BsonElement("coord")]
    public double[]? Coordinates { get; set; }

    public string? Street { get; set; }

    [BsonElement("zipcode")]
    public string? ZipCode { get; set; }

}
